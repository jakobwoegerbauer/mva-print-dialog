import { QMainWindow } from "@nodegui/nodegui";
import { PrintWidget } from "./widgets/print";

export class PrintDialog {

    window: QMainWindow;

    constructor() {
        this.window = new QMainWindow();
        this.window.setCentralWidget(new PrintWidget());
        this.window.resize(800, 600);
        this.window.setStyleSheet(
            `
            #myroot {
                background-color: #009688;
            }
            `
        );
        this.window.show();
    }
}