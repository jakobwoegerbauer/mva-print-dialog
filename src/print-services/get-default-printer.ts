import execAsync from "./exec-async";
import parsePrinterAttribute from "./parse-printer-attr";
import { KEYWORDS, Printer } from "./printer";

export default async function getDefaultPrinter(): Promise<Printer | null> {
  try {
    const { stdout } = await execAsync("lpstat -d");
    const printer = getPrinterName(stdout);
    if (!printer) return null;
    return await getPrinterData(printer);
  } catch (error) {
    throw error;
  }
}

function getPrinterName(output: string): string {
  const startIndex = output.indexOf(":");
  return startIndex === -1 ? "" : output.slice(startIndex + 1).trim();
}

async function getPrinterData(printer: string): Promise<Printer> {
  const { stdout } = await execAsync(`lpstat -lp ${printer}`);
  return {
    printer,
    status: stdout.split(/.*is\s(\w+)\..*/gm)[1],
    description: parsePrinterAttribute(stdout, KEYWORDS.description),
    alerts: parsePrinterAttribute(stdout, KEYWORDS.alerts),
    connection: parsePrinterAttribute(stdout, KEYWORDS.connection),
  };
}
