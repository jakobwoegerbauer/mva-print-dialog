export interface Printer {
    printer: string;
    description: string | null;
    status: string | null;
    alerts: string | null;
    connection: string | null;
  }
  
export const KEYWORDS = {
  printer: 'Drucker',
  description: 'Beschreibung',
  alerts: 'Alarme',
  connection: 'Verbindung'
};