import execAsync from "./exec-async";
import parsePrinterAttribute from "./parse-printer-attr";
import { KEYWORDS, Printer } from "./printer";

export default async function getPrinters(): Promise<Printer[]> {
  try {
    const { stdout, stderr } = await execAsync("lpstat -lp");

    const isThereAnyPrinter = stdout.match(KEYWORDS.printer);
    if (!isThereAnyPrinter) return [];

    const printerSplitRegex = new RegExp(`^${KEYWORDS.printer}(.)`, 'gm');

    return stdout
      .split(printerSplitRegex)
      .filter((line: string) => line.trim().length)
      .map((line: string) => ({
        printer: line.slice(0, line.indexOf(" ")),
        status: line.split(/.*is\s(\w+)\..*/gm)[1],
        description: parsePrinterAttribute(line, KEYWORDS.description),
        alerts: parsePrinterAttribute(line, KEYWORDS.alerts),
        connection: parsePrinterAttribute(line, KEYWORDS.alerts),
      }));
  } catch (error) {
    throw error;
  }
}
