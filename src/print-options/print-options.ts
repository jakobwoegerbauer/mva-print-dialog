import { MediaSize, NumberUp, Orientation, PageList, PAGES_PATTERN, Sides } from "./option-types";

class SelectOption<T> {
    name: string;
    value: T;
    constructor(name: string, value: T) {
        this.name = name;
        this.value = value;
    }
}

export const MediaSizeOptions = {
    a4: new SelectOption<MediaSize>('A4', 'a4'),
    a3: new SelectOption<MediaSize>('A3', 'a3')
};

export const OrientationOptions = {
    portrait: new SelectOption<Orientation>('Hochformat', 'portrait'),
    landscape: new SelectOption<Orientation>('Querformat', 'landscape')
};

export const SidesOptions = {
    oneSided: new SelectOption<Sides>('Einseitig', 'one-sided'),
    twoSidedLongEdge: new SelectOption<Sides>('Doppelseitig (lange Seite)', 'two-sided-long-edge'),
    twoSidedShortEdge: new SelectOption<Sides>('Doppelseitig (kurze Seite)', 'two-sided-short-edge')
};

export const NumberUpOptions = {
    n1: new SelectOption<NumberUp>('Nicht kombinieren', 1),
    n2: new SelectOption<NumberUp>('2 auf eine Seite', 2),
    n4: new SelectOption<NumberUp>('4 auf eine Seite', 4),
    n6: new SelectOption<NumberUp>('6 auf eine Seite', 6),
    n9: new SelectOption<NumberUp>('9 auf eine Seite', 9),
    n16: new SelectOption<NumberUp>('16 auf eine Seite', 16)
};

type ValidationResult = boolean | string;

export function validatePageList(pageList: PageList): ValidationResult {
    if(pageList && !PAGES_PATTERN.test(pageList)) {
        return 'Falsches Format. Beispiel: 2,10,20-28';
    }
    return true;
}

export function validateCopies(copies: number): ValidationResult {
    return copies >= 1 && copies < 1000 || 'Wert muss zwischen 1 und 1000 liegen.';
}

export class PrintOptions {
    copies: number = 1;
    pageList: PageList = null;
    
    media = MediaSizeOptions.a4;
    orientation = OrientationOptions.portrait;
    sides = SidesOptions.twoSidedLongEdge;
    fitToPage: boolean = true;
    numberUp = NumberUpOptions.n1;

    constructor(data?: Partial<PrintOptions>) {
        Object.assign(this, data);
    }

    validate(): boolean | string {
        return validatePageList(this.pageList)
         && validateCopies(this.copies);
    }

    toOptionsArray(): string[] {
        if(!this.validate()) {
            throw new Error('options are invalid!');
        }

        const arr:string[] = [];

        arr.push(`-n ${this.copies}`);
        this.pageList != null && arr.push(`-P "${this.pageList}"`);

        arr.push(`-o media=${this.media.value}`);
        this.orientation.value == "landscape" && arr.push(`-o landscape`);
        arr.push(`-o sides=${this.sides.value}`);
        this.fitToPage && arr.push(`-o fit-to-page`);
        this.numberUp.value != 1 && arr.push(`-o number-up=${this.numberUp}`);

        return arr;
    }
}

export class PrintOptionsPreset extends PrintOptions {
    name: string;;
    constructor(name: string, data?: Partial<PrintOptions>) {
        super(data);
        this.name = name;
    }
}