export type MediaSize = 'a3' | 'a4';
export type Orientation = 'portrait' | 'landscape';
export type Sides = 'one-sided' | 'two-sided-long-edge' | 'two-sided-short-edge';
export type NumberUp = 1 | 2 | 4 | 6 | 9 | 16;
export type PageList = null | string;

export const PAGES_PATTERN = /^\d+(?:-\d+)?(?:,\h*\d+(?:-\d+)?)*$/;