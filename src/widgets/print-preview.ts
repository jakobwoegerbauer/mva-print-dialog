import { QWidget, FlexLayout, QLabel, QComboBox, QVariant } from "@nodegui/nodegui";

class asdf {
    build() {

        const centralWidget = new QWidget();
        centralWidget.setObjectName("myroot");
        const rootLayout = new FlexLayout();
        centralWidget.setLayout(rootLayout);

        const label = new QLabel();
        label.setInlineStyle("font-size: 16px; font-weight: bold;");
        label.setText("Hello World");

        const comboBox = new QComboBox();
        comboBox.addItem(null, "test", new QVariant("sdf"));

        rootLayout.addWidget(label);
        rootLayout.addWidget(comboBox);
    }
}