import { NativeElement, QGridLayout, QInputDialog, QLabel, QLayout, QWidget, QWidgetSignals } from "@nodegui/nodegui";

export class PrintWidget extends QWidget {

    rootLayout: QLayout;

    label: QLabel;
    
    constructor(arg?: QWidget<QWidgetSignals> | NativeElement) {
        super(arg);
        this.create();
    }

    create() {
        this.rootLayout = new QGridLayout();
        
        this.label = new QLabel();
        this.label.setText('Test label');
        this.rootLayout.addWidget(this.label);
        this.setLayout(this.rootLayout);
    }
}